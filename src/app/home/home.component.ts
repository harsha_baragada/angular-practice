import { Post } from './../posts/posts.component';
import { PostService } from './../services/posts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  title: string = 'Welcome to my angular application';
  userName!: string;
  password!: string;
  isUserNameError: boolean = false;
  isPasswordError: boolean = false;
  fruits: string[] = ['Mango', 'Apple', 'Banana', 'Orange', 'Papaya'];
  posts: Post[] = [];
  constructor(private postService: PostService) {}

  ngOnInit(): void {
    this.postService.getPosts().subscribe((posts) => {
      this.posts = posts;
      console.log(this.posts);
    });
  }

  submit() {
    this.resetErrors();
    if (!this.userName) {
      this.isUserNameError = true;
    }
    if (!this.password) {
      this.isPasswordError = true;
    }
  }

  resetErrors() {
    this.isUserNameError = false;
    this.isPasswordError = false;
  }
  userLike(event: any) {
    alert(event);
  }

  userShared(event: Post) {
    alert(event.id + ' ' + event.body);
  }
}
