import { PostService } from './../services/posts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  posts!: Post[];
  displayedColumns: string[] = ['userId', 'id', 'title', 'body'];
  constructor(private service: PostService) {}

  ngOnInit(): void {
    this.service.getPosts().subscribe((posts) => {
      this.posts = posts;
    });
  }
}

export interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}
