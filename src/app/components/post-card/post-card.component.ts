import { Post } from './../../posts/posts.component';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css'],
})
export class PostCardComponent implements OnInit {
  @Input() post!: Post;
  @Output() liked = new EventEmitter();
  @Output() shared = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  likeButtonClicked() {
    this.liked.emit(this.post.id + ' liked');
  }

  sharedPost() {
    this.shared.emit(this.post);
  }
}
